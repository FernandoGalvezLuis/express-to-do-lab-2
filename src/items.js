import express from 'express'
const router = express.Router()
const items = [{id: 1, task: "buy groceries"}, {id:2, task: "clean room"}, {id: 3, task: "another task"}, {id: 4, task: "thisTask"}]

router.get('/', (req, res) => {
    return res.json(items)
})

router.post('/', (req, res) => {
    const requiredProperties = ['id', 'task']
    let missingProperties = []
    requiredProperties.forEach(prop => {
        if (!req.body.hasOwnProperty(prop)) {
            missingProperties.push(prop)
        }
    })
    if (missingProperties.length) { // we do not need a specific comparison here because a 0 is essentially the same as false
        // we have missing properties!
        let errorMessage = []
        missingProperties.forEach(prop => {
            errorMessage.push(`Missing property: ${prop}`)
        })
        return res.status(400).json({ errors: errorMessage })
    }

    items.push(req.body)
    return res.status(201).json(req.body)
})

router.get('/:id', (req, res) => {

    let index= items.find(a=> a.id == req.params.id)

    return res.status(200).send(`This is what I should get: ${JSON.stringify(index)}`)   

    //return res.send(`You just requested item that should be with id ${req.params.id}`) // we are using the send function here because we are not returning an object but only a string
})


router.put('/:id', (req, res)=> {
    const requiredProperties = ['id', 'task']
    let missingProperties = []

    requiredProperties.forEach(prop => {
        if (!req.body.hasOwnProperty(prop)) {
            missingProperties.push(prop)
        }
    })

    if (missingProperties.length) { // we do not need a specific comparison here because a 0 is essentially the same as false
        // we have missing properties!
        let errorMessage = []
        missingProperties.forEach(prop => {
            errorMessage.push(`Missing property: ${prop}`)
        })
        return res.status(400).json({ errors: errorMessage })
    }

    if(items.indexOf(req.body.id)!=-1){
        return res.status(404).json(`No object in items array with id: ${req.body.id}`)
    }


    if(req.body.id != req.params.id){
        return res.status(400).json(`Bad request id URL and request doesnt match `)
    }


    let obj= items.find(a=> a.id == req.params.id) //Bj_that_matches_req_id

    let index=items.findIndex(a=> a.id==obj.id) //Index in items array of obj

     items[index].task = req.body.task

   //items.splice(fIndex,1,upDatedOBJ)

    res.status(201).send(`This is what I should get: ${JSON.stringify(items[index])}`)
})



router.delete('/:id', (req, res)=>{

    const requiredProperties = ['id', 'task']
    let missingProperties = []

    requiredProperties.forEach(prop => {
        if (!req.body.hasOwnProperty(prop)) {
            missingProperties.push(prop)
        }
    })

    if (missingProperties.length) { // we do not need a specific comparison here because a 0 is essentially the same as false
        // we have missing properties!
        let errorMessage = []
        missingProperties.forEach(prop => {
            errorMessage.push(`Missing property: ${prop}`)
        })
        return res.status(400).json({ errors: errorMessage })
    }

    if(items.indexOf(req.body.id)!=-1){
        return res.status(404).json(`No object in items array with id: ${req.body.id}`)
    }


    if(req.body.id != req.params.id){
        return res.status(400).json(`Bad request id URL and request doesnt match `)
    }





    let obj= items.find(a=> a.id == req.params.id) //OBj_that_matches_req_id

    let index=items.findIndex(a=> a.id==obj.id) //Index in items array of obj

     items.splice(index,1)

    return res.status(204).send(`Something here: ${JSON.stringify(items)}`)
})



router.patch('/:id', (req,res)=>{

    if(req.body.id != req.params.id){
        return res.status(400).json(`Bad request id URL and request doesnt match `)
    } //Make sure we are targeting the correct item bases on id matching req.body (what´s coming from user) 
    //and whats inside DB (that should be in req.params) that whould be deal with in final res if it matches
    //if not res will send a 400 status

  /*  if(items.indexOf(req.body.task)!=-1){
        return res.status(404).json(`No object in items array with task: ${req.body.task}`)
    }//Make sure we have a task with that name within DB  */

    let obj= items.find(a=> a.id == req.params.id) //OBj_that_matches_req_id

    let index=items.findIndex(a=> a.id==obj.id) //Index in items array of obj

    if(req.body.task!=items[index].task){
        items[index].task = req.body.task
        return res.status(200).send(items[index])
    }

    return res.status(418).send(`What are you doing? 
    Review your request, are you trying to PATCH the old TASK with the same TASK? That doen't make sense`)

})


export default router
